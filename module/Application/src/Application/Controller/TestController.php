<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';

        $this->cache = StorageFactory::factory(array(
            'adapter' => array(
                'name'    => 'apc',
                'options' => array('ttl' => $this->cacheTime),
            ),
            'plugins' => array(
                'exception_handler' => array('throw_exceptions' => false),
            )
        ));
    }
################################################################################
    private function findAnswer1($input){
        $output = $this->cache->getItem('answer1:'.$input);
        // var_dump($output);

        if(!isset($output)){
            $output = 0;
            for ($i = 1; $i <= $input; $i++) {
                $output += ($i - 1) * 2;
            }
            $output += 3;

            $this->cache->setItem('answer1:'.$input, $output);
        }

        return $output;
    }
    private function findAnswer2($input){
        $output = $this->cache->getItem('answer2:'.$input);
        // var_dump($output);

        if(!isset($output)){
            $output = 0;
            $output = $input - 20 - 16;

            $this->cache->setItem('answer2:'.$input, $output);
        }
        
        return $output;
    }
    public function indexAction(){
        $view = new ViewModel();

        $answers = $this->cache->getItem('test_answers');
        // var_dump($answers);

        if(!isset($answers)){
            $answers = [
                $this->findAnswer1(5),
                $this->findAnswer2(99)
            ];
            $view->is_cache = false;

            $this->cache->setItem('test_answers', $answers);
        }else{
            $view->is_cache = true;
        }
        
        $view->answers = $answers;

        return $view;
    }
}
################################################################################